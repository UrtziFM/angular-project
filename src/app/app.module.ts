import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { BluechipsComponent } from './components/bluechips/bluechips.component';
// import { BigtechsComponent } from './components/bigtechs/bigtechs.component';
import { DataViewComponent } from './components/data-view/data-view.component';
import { DataFormComponent } from './components/data-view/data-form/data-form.component';
import { DataListComponent } from './components/data-view/data-list/data-list.component';
// import { MainInputComponent } from './components/main-input/main-input.component';
// import { ReactiveFormComponent } from './components/reactive-form/reactive-form.component';

@NgModule({
  declarations: [AppComponent, DataViewComponent, DataFormComponent, DataListComponent],
  // BluechipsComponent, BigtechsComponent, DataViewComponent, DataFormComponent, DataListComponent],
  // MainInputComponent, ReactiveFormComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
