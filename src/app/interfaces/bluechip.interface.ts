export interface IBluechip {
    name: string;
    image: string;
    prize: string;
  }
