export const apiResponse = [
    {
      name: 'Inditex',
      image: 'http://www.consumoconcausa.org/wp-content/uploads/cache/images/th-1/th-1--807765919.jpeg',
      prize: '85.71 mil M'
    }, 
    { 
      name: 'Santander',
      image: 'http://www.brandemia.org/sites/default/files/santander_logo_copia.jpg',
      prize: '60.02 mil M'
    },
    {
      name: 'Iberdrola',
      image: 'https://www.vectorlogo.es/wp-content/uploads/2015/02/logo-vector-iberdrola.jpg',
      prize: '56.46 mil M'
    },
    {
      name: 'Telefonica',
      image: 'https://www.vectorlogo.es/wp-content/uploads/2015/01/logo-vector-telefonica.jpg',
      prize: '35.12 mil M'
    },
    {
      name: 'BBVA',
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjGyBnTeO89NKxy5Q2QLaR2Qe5ruU8HkH-tZhRqOA76nr90NbF&s',
      prize: '32.05 mil M'
    },
  ];
