export const apiResponseBig = [
    {
      name: 'Apple',
      image: 'https://static.vecteezy.com/system/resources/previews/000/064/720/non_2x/apple-vector-logo.jpg',
      prize: '1.160 mil M'
      },
    {
      name: 'Microsoft',
      image: 'https://www.softzone.es/app/uploads-softzone.es/2012/08/Microsoft_Logo.png',
      prize: '1.140 mil M'
    },
    {
      name: 'Google',
      image: 'https://img-cdn.hipertextual.com/files/2015/09/google-logo-2013-610x343.jpg?strip=all&lossy=1&quality=57',
      prize: '891.21 mil M'
      }, 
    { 
      name: 'Amazon',
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRTfkIEEPbrQ_0h-HzwXgf77mpiRdJk1RFa8ibeQtAoxHsFhxaa&s',
      prize: '863.23 mil M'
    },
    {
      name: 'Facebook',
      image: 'https://cdn.worldvectorlogo.com/logos/facebook.svg',
      prize: '566.33 mil M'
    },
  ];
