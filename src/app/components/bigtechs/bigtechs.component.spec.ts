import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BigtechsComponent } from './bigtechs.component';

describe('BigtechsComponent', () => {
  let component: BigtechsComponent;
  let fixture: ComponentFixture<BigtechsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BigtechsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BigtechsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});