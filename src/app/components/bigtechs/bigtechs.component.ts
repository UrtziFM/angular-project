import { Component, OnInit } from '@angular/core';

import { IBigtech } from '../../interfaces/bigtech.interface';
import { apiResponseBig } from '../../api/bigtechs.api';

@Component({
  selector: 'app-bigtechs',
  templateUrl: './bigtechs.component.html',
  styleUrls: ['./bigtechs.component.scss']
})
export class BigtechsComponent implements OnInit {
  public bigtechs: IBigtech[];

  constructor() {
    // Give a default initial value to the variable to prevent breaking the render
    this.bigtechs = [];
  }

  ngOnInit() {
    // If this was a request, the place to set the variable
    // is ngOnInit to prevent delayed rendering
    this.bigtechs = apiResponseBig;
  }
}