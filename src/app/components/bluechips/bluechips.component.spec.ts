import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BluechipsComponent } from './bluechips.component';

describe('BluechipsComponent', () => {
  let component: BluechipsComponent;
  let fixture: ComponentFixture<BluechipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluechipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BluechipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
