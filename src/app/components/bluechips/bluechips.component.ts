import { Component, OnInit } from '@angular/core';

import { IBluechip } from '../../interfaces/bluechip.interface';
import { apiResponse } from '../../api/bluechips.api';

@Component({
  selector: 'app-bluechips',
  templateUrl: './bluechips.component.html',
  styleUrls: ['./bluechips.component.scss']
})
export class BluechipsComponent implements OnInit {
  public bluechips: IBluechip[];

  constructor() {
    // Give a default initial value to the variable to prevent breaking the render
    this.bluechips = [];
  }

  ngOnInit() {
    // If this was a request, the place to set the variable
    // is ngOnInit to prevent delayed rendering
    this.bluechips = apiResponse;
  }
}
