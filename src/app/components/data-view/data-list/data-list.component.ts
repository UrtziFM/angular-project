import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss']
})
export class DataListComponent implements OnInit {
  @Input() name: string;

  constructor() { }

  ngOnInit() {
    console.log('The input is', name);
  }

}
