import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-data-form',
  templateUrl: './data-form.component.html',
  styleUrls: ['./data-form.component.scss']
})
export class DataFormComponent implements OnInit {
  @Output() emitMessage = new EventEmitter<string>();
  message: string = '';

  constructor() { }

  ngOnInit() {
  }
  sendMessage(){
    this.emitMessage.emit(this.message);
  }

}
