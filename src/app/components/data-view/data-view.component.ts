import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-view',
  templateUrl: './data-view.component.html',
  styleUrls: ['./data-view.component.scss']
})
export class DataViewComponent implements OnInit {
  outputMessage: string = '';

  constructor() { }

  ngOnInit() {
  }
  setMessage(message: string): void {
    this.outputMessage = message;
  } 

}
