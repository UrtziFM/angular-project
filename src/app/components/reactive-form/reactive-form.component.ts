import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {
  custForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
  ) { 
    this.custForm = this.formBuilder.group({
      name: '',
      age: '',
    });
  }

  ngOnInit() {}

  onSubmit(formValue){
    console.log(formValue);
    this.custForm.reset();
  }

}
